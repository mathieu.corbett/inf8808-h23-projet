import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
const d3v6Tip = require('d3-v6-tip');
const { tip } = d3v6Tip;

@Component({
  selector: 'app-bar-chart1',
  templateUrl: './bar-chart1.component.html',
  styleUrls: ['./bar-chart1.component.scss'],
})
export class BarChart1Component implements OnInit {
  private tooltip: any;
  private svg: any;
  private margin = 50;
  private width = 400 - this.margin * 2;
  private height = 300 - this.margin * 2;

  constructor() {}

  ngOnInit(): void {
    this.createSvg();
    this.drawBars();
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#bar1')
      .append('svg')
      .attr('width', this.width + this.margin * 2)
      .attr('height', this.height + this.margin * 2)
      .append('g')
      .attr('transform', 'translate(' + this.margin + ',' + this.margin + ')');

    this.tooltip = tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html((e: any, d: any) => {
        return '<b style=\'font-size: 24px; font-weight: normal\'>' +
        this.getAircraftType(d) + "<br/>" +
        "<p>" + d.count + " appareil" + (d.count > 1 ? "s" : "") + "</p>";
      });
    
    this.svg.call(this.tooltip);
  }

  private getAircraftType(d: any): string {
    let aircraftType: string = "";
    let numberEngines: number;
    let propulsionType: string = "";
    
    switch ((d.type as string)[0]) {
      case "L":
        aircraftType = "Avions";
        break;
      case "S":
        aircraftType = "Hydravions";
        break;
      case "A":
        aircraftType = "Aéronefs amphibies";
        break;
      case "G":
        aircraftType = "Autogires";
        break;
      case "H":
        aircraftType = "Hélicoptères";
        break;
      case "T":
        aircraftType = "Tiltrotors";
        break;
    }

    numberEngines = Number.parseInt((d.type as string)[1]);

    switch ((d.type as string)[2]) {
      case "J":
        propulsionType = "moteur" + (numberEngines > 1 ? "s" : "") + " à réaction";
        break;
      case "T":
        propulsionType = "turbopropulseur" + (numberEngines > 1 ? "s" : "");
        break;
      case "P":
        propulsionType = "moteur" + (numberEngines > 1 ? "s" : "") + " à pistons";
        break;
      case "E":
        propulsionType = "moteur" + (numberEngines > 1 ? "s" : "") + " électrique" + (numberEngines > 1 ? "s" : "");
        break;
      case "F":
        propulsionType = "moteur" + (numberEngines > 1 ? "s" : "") + "-fusée" + (numberEngines > 1 ? "s" : "");
        break;
    }
    
    return aircraftType + " propulsés par " + numberEngines + " " + propulsionType;
  }

  private drawBars(): void {
    d3.json<Iterable<{ type: string, count: number }>>('./assets/data/planetypes.json')
      .then((data) => {
        if (data === undefined || data === null) {
          data = [];
        }
        const maxTypes = d3.max(d3.map(data, (d) => d.count)) ?? 0;
        
        // Create the X-axis band scale
        const x = d3
          .scaleBand()
          .range([0, this.width])
          .domain(d3.map(data, (d) => d.type))
          .padding(0.2);

        // Draw the X-axis on the DOM
        this.svg
          .append('g')
          .attr('transform', 'translate(0,' + this.height + ')')
          .call(d3.axisBottom(x))
          .selectAll('text')
          .attr('transform', 'translate(-10,0)rotate(-45)')
          .style('text-anchor', 'end');

        // Create the Y-axis band scale
        const y = d3.scaleLinear().domain([0, 200000]).range([this.height, 0])
        .domain([0, maxTypes]);

        // Draw the Y-axis on the DOM
        this.svg.append('g').call(d3.axisLeft(y));

        // Create and fill the bars
        this.svg
          .selectAll('bars')
          .data(data)
          .enter()
          .append('rect')
          .attr('x', (d: any) => x(d.type))
          .attr('y', (d: any) => y(d.count))
          .attr('width', x.bandwidth())
          .attr('height', (d: any) => this.height - y(d.count))
          .attr('fill', '#1f77b3')
          .on('mouseenter', (e: any, d: any) => this.tooltip.show(e, d))
          .on('mouseleave', (e: any, d: any) => this.tooltip.hide());
    });
  }
}