import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
const d3v6Tip = require('d3-v6-tip');
const { tip } = d3v6Tip;

@Component({
  selector: 'app-bar-chart2',
  templateUrl: './bar-chart2.component.html',
  styleUrls: ['./bar-chart2.component.scss'],
})
export class BarChart2Component implements OnInit {
  private tooltip: any;
  private svg: any;
  private margin = 100;
  private width = 400 - this.margin;
  private height = 300 - this.margin;
  private privateData = d3.json<Iterable<{ model: string, count: number }>>('./assets/data/modelesprives.json');
  private commercialData = d3.json<Iterable<{ model: string, count: number }>>('./assets/data/modelespublic.json')
  private modelData:Promise<Iterable<{ model: string; count: number; }> | undefined> = this.privateData;
  public isShowingPrivate: boolean = true;
  private chartContainerIdentifier = 'figure#bar2';

  constructor() {}

  ngOnInit(): void {
    this.createSvg();
    this.drawBars();
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#bar2')
      .append('svg')
      .attr('width', this.width + this.margin * 2)
      .attr('height', this.height + this.margin * 2.05)
      .append('g')
      .attr('transform', 'translate(' + this.margin + ', 15)');

    this.tooltip = tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html((e: any, d: any) => {
        return (
          "<b style='font-size: 24px; font-weight: normal'>" +
          d.model +
          '<br/>' +
          '<p>' +
          d.count +
          ' appareil' +
          (d.count > 1 ? 's' : '') +
          '</p>'
        );
      });

    this.svg.call(this.tooltip);
  }

  private drawBars(): void {
    if (this.isShowingPrivate) {
      d3.json<Iterable<{ model: string; count: number }>>(
        './assets/data/modelesprive.json'
      ).then((data) => {
        if (data === undefined || data === null) {
          data = [];
        }
        const maxTypes = d3.max(d3.map(data, (d) => d.count)) ?? 0;

        // Create the X-axis band scale
        const x = d3
          .scaleBand()
          .range([0, this.width])
          .domain(d3.map(data, (d) => d.model))
          .padding(0.2);

        // Draw the X-axis on the DOM
        this.svg
          .append('g')
          .attr('transform', 'translate(0,' + this.height + ')')
          .call(d3.axisBottom(x))
          .selectAll('text')
          .attr('transform', 'translate(-10,0)rotate(-30)')
          .style('text-anchor', 'end');

        // Create the Y-axis band scale
        const y = d3
          .scaleLinear()
          .domain([0, 200000])
          .range([this.height, 0])
          .domain([0, maxTypes]);

        // Draw the Y-axis on the DOM
        this.svg.append('g').call(d3.axisLeft(y));

        // Create and fill the bars
        this.svg
          .selectAll('bars')
          .data(data)
          .enter()
          .append('rect')
          .attr('x', (d: any) => x(d.model))
          .attr('y', (d: any) => y(d.count))
          .attr('width', x.bandwidth())
          .attr('height', (d: any) => this.height - y(d.count))
          .attr('fill', '#1f77b3')
          .on('mouseenter', (e: any, d: any) => this.tooltip.show(e, d))
          .on('mouseleave', (e: any, d: any) => this.tooltip.hide());
      });
    } else {
      d3.json<Iterable<{ model: string; count: number }>>(
        './assets/data/modelespublic.json'
      ).then((data) => {
        if (data === undefined || data === null) {
          data = [];
        }
        const maxTypes = d3.max(d3.map(data, (d) => d.count)) ?? 0;

        // Create the X-axis band scale
        const x = d3
          .scaleBand()
          .range([0, this.width])
          .domain(d3.map(data, (d) => d.model))
          .padding(0.2);

        // Draw the X-axis on the DOM
        this.svg
          .append('g')
          .attr('transform', 'translate(0,' + this.height + ')')
          .call(d3.axisBottom(x))
          .selectAll('text')
          .attr('transform', 'translate(-10,0)rotate(-30)')
          .style('text-anchor', 'end');

        // Create the Y-axis band scale
        const y = d3
          .scaleLinear()
          .domain([0, 200000])
          .range([this.height, 0])
          .domain([0, maxTypes]);

        // Draw the Y-axis on the DOM
        this.svg.append('g').call(d3.axisLeft(y));

        // Create and fill the bars
        this.svg
          .selectAll('bars')
          .data(data)
          .enter()
          .append('rect')
          .attr('x', (d: any) => x(d.model))
          .attr('y', (d: any) => y(d.count))
          .attr('width', x.bandwidth())
          .attr('height', (d: any) => this.height - y(d.count))
          .attr('fill', '#f2980e')
          .on('mouseenter', (e: any, d: any) => this.tooltip.show(e, d))
          .on('mouseleave', (e: any, d: any) => this.tooltip.hide());
      });
    }
  }

  public switchData(toPrivate: boolean): void {
    if (toPrivate && !this.isShowingPrivate) {
      this.isShowingPrivate = true;

      this.modelData = this.privateData;
      document.getElementById('bar2')?.replaceChildren();
      this.createSvg();
      this.drawBars();
    } else if (!toPrivate && this.isShowingPrivate) {
      this.isShowingPrivate = false;

      this.modelData = this.commercialData;
      document.getElementById('bar2')?.replaceChildren();
      this.createSvg();
      this.drawBars();
    }
  }
}