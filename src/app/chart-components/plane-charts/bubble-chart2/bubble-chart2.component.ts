import { Component, Input, OnInit } from '@angular/core';
import * as d3 from 'd3';
import Reveal from 'reveal.js';
import {legendColor} from 'd3-svg-legend'
const d3v6Tip = require('d3-v6-tip');
const { tip } = d3v6Tip;

@Component({
  selector: 'app-bubble-chart2',
  templateUrl: './bubble-chart2.component.html',
  styleUrls: ['./bubble-chart2.component.scss']
})
export class BubbleChart2Component implements OnInit {
  @Input() SlideNumber: number = 0;
  private tooltip: any;
  private width = 750
  private height = 500
  private counts : any
  private countExtent : any
  private circleRadiusScale : any
  private minCircleRadius = 15
  private maxCircleRadius = 50
  private colorScale = d3.scaleOrdinal(d3.schemeCategory10)
  
  constructor() {
    d3.json('./assets/data/proprietaireCount.json').then( (data) => {
      this.createBubbleChart(data);
    })
  }

  ngOnInit(): void {

  }

  private createBubbleChart(data: any): void {
    this.tooltip = tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html((e:any, d: any) => this.getToolTipContent(e, d));

    this.counts = data.map((d: any) => +d.count)
    this.countExtent = d3.extent(this.counts)
  
    this.circleRadiusScale = d3.scaleSqrt()
    .domain(this.countExtent)
    .range([this.minCircleRadius, this.maxCircleRadius]);
  
    d3.select('#owner-bubble-chart-svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .call(this.tooltip);
  
    d3.select('#bubble-chart-2-g')
    .selectAll('circle')
    .data(data)
    .join('circle')
    .attr('r', (d: any) => this.circleRadiusScale(d.count))
    .attr('fill', (d: any) => this.colorScale(d.continent))
    .attr('stroke', 'white')
    .attr('opacity', 0.7)
    .attr('fillOpacity', 0.7)
    .attr('cursor', 'pointer')
    .on('mouseenter', (e: Event, d: any) => {
      if (e.target) {
        (e.target as SVGCircleElement).style.fillOpacity = '1';
        (e.target as SVGCircleElement).style.opacity = '1';
      }
      this.tooltip.show(e, d);
    })
    .on('mouseleave', (e:any, d:any) => {
      if (e.target) {
        (e.target as SVGCircleElement).style.fillOpacity = '0.7';
        (e.target as SVGCircleElement).style.opacity = '0.7';
      }
      this.tooltip.hide();
    });
  
    this.createForceSimulation(data)
    
    this.drawLegend()
  
  }
  
  private createContinentForces() {
    
    let continentForceX = (d : any) => {
      if (d.continent === 'Amérique du Nord') {
        return left(this.width);
      } else if (d.continent === 'Europe') {
        return left(this.width);
      }
      return center(this.width);
    }
    
    let continentForceY = (d : any) => {
      if (d.continent === 'Amérique du Nord') {
        return top(this.height);
      } else if (d.continent === 'Europe') {
        return bottom(this.height);
      }
      return center(this.height);
    }
    
    function left(dimension: any) { return dimension / 2; }
    function center(dimension: any) { return dimension / 2; }
    function top(dimension: any) { return dimension / 2; }
    function bottom(dimension: any) { return dimension / 4 * 1.2; }
    
    return {
      x: d3.forceX(continentForceX).strength(10),
      y: d3.forceY(continentForceY).strength(10)
    };
  }
  
  private createForceSimulation(data: any) {
    var forceSimulation = d3.forceSimulation(data)
    .force('charge', d3.forceManyBody().strength(5))
    .force('x', this.createContinentForces().x)
    .force('y', this.createContinentForces().y)
    .force('collision', d3.forceCollide( (d: any) => this.circleRadiusScale(d.count) * 1.05 ).strength(1))

    forceSimulation.stop();
  
    forceSimulation.nodes(data)
        .on('tick', () => {
          d3.select('#bubble-chart-2-g')
          .selectAll('circle')
          .attr('cx', (d: any) => d.x )
          .attr('cy', (d: any) => d.y )
        })
    
    Reveal.addEventListener('slidetransitionend', (event: any) => {
      if (event.currentSlide.dataset.slidenumber == this.SlideNumber) {
        forceSimulation.restart();
      }
    });
  }
  
  private drawLegend () {
  
    d3.select('#bubble-chart-2-legend-g')
    .append('svg')
    .attr('id', 'bubble-chart-2-legend')
  
    var svg = d3.select('#bubble-chart-2-legend');
  
    svg.append('g')
    .attr('id', 'bubble-chart-2-legendOrdinal')
    .attr('transform', 'translate( 500, 25)');
  
    var legendOrdinal: any = legendColor()
    .shape('circle')
    .shapeRadius(5)
    .title('Légende')
    .scale(this.colorScale)
  
    svg.select('#bubble-chart-2-legendOrdinal')
    .call(legendOrdinal);
  
  }

  private getToolTipContent(e: any, d: any): string {
    const title =
      '<b style=\'font-size: 24px; font-weight: normal\'>' +
      d.proprietaire +
      '</b>';
    const subTitle = `<p>Le propriétaire possède ${d.count} avions</p>`;

    return title + '</br>' + subTitle;
  }
  
}