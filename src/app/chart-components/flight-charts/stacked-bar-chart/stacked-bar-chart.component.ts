import { Component, OnInit } from '@angular/core';
import { DecimalPipe, formatNumber } from '@angular/common';
import * as d3 from 'd3';
const d3v6Tip = require('d3-v6-tip');
const { tip } = d3v6Tip;

@Component({
  selector: 'app-stacked-bar-chart',
  templateUrl: './stacked-bar-chart.component.html',
  styleUrls: ['./stacked-bar-chart.component.scss'],
})
export class StackedBarChartComponent implements OnInit {
  private svg: any;
  private margin: number = 50;
  private width: number = 375 - this.margin * 2;
  private height: number = 300 - this.margin * 2;

  private tooltip: any;

  constructor() {}

  ngOnInit(): void {
    this.createSvg();
    this.drawGroupBar();
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#stackedBarChartPrive')
      .attr('style', 'margin-top: 0px')
      .attr('style', 'margin-bottom: 0px')
      .append('svg')
      .attr('width', this.width + this.margin * 2)
      .attr('height', this.height + this.margin * 2)
      .append('g')
      .attr('transform', 'translate(' + this.margin + ',' + this.margin + ')');

    this.tooltip = tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html((e: any, d: any) => this.getToolTipContent(e, d));

    this.svg.call(this.tooltip);
  }

  private drawGroupBar(): void {
    d3.json<
      Array<{
        mois: string;
        nombreprive: number;
        nombrepublic: number;
        percentPrive: number;
        percentPublic: number;
      }>
    >('./assets/data/volParMois.json').then((data) => {
      if (data === undefined || data === null) {
        return;
      }
      const x = this.setXScale(data);
      this.drawXAxis(x);

      const y = this.setYScale();
      this.drawYAxis(y);

      const colorScale = this.setColorScale();

      var percentData = [{}];

      data.forEach((element, i) => {
        data[i].percentPrive =
          (data[i].nombreprive / (data[i].nombreprive + data[i].nombrepublic)) *
          100;
        data[i].percentPublic = 100 - data[i].percentPrive;

        percentData[i] = {
          mois: data[i].mois,
          percentPrive: data[i].percentPrive,
          percentPublic: data[i].percentPublic,
        };
      });

      let stackedData = d3.stack().keys(['percentPrive', 'percentPublic'])(
        percentData
      );

      // Create and fill the bars
      this.fillStackBar(stackedData, colorScale, x, y);

      // Add the legend
      this.setLegend(stackedData, colorScale);
    });
  }

  private setXScale(data: any): any {
    return d3
      .scaleBand()
      .range([0, this.width])
      .domain(data.map((d: { mois: any }) => d.mois))
      .padding(0.2);
  }

  private setYScale(): any {
    return d3.scaleLinear().domain([0, 100]).range([this.height, 0]);
  }

  private setColorScale(): any {
    return d3
      .scaleOrdinal()
      .domain(['percentPrive', 'percentPublic'])
      .range(['#f2980e', '#1f77b3']);
  }

  private drawXAxis(x: any): void {
    this.svg.selectAll('g.axisXStackedBarChart').remove();

    this.svg
      .append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('class', 'axisXStackedBarChart')
      .call(d3.axisBottom(x))
      .selectAll('text')
      .attr('transform', 'translate(-10,0)rotate(-45)')
      .style('text-anchor', 'end');
  }

  private drawYAxis(y: any): void {
    this.svg.select('axisYStackedBarChart').remove();

    this.svg
      .append('g')
      .call(d3.axisLeft(y))
      .attr('class', 'axisYStackedBarChart');
  }

  private fillStackBar(
    stackedData: any,
    colorScale: any,
    x: any,
    y: any
  ): void {
    this.svg
      .append('g')
      .selectAll('g')
      .data(stackedData)
      .enter()
      .append('g')
      .attr('fill', (d: any) => colorScale(d.key))
      .selectAll('rect')
      .data((d: any) => d)
      .enter()
      .append('rect')
      .attr('x', (d: any) => x(d.data.mois))
      .attr('y', (d: any[]) => y(d[1]))
      .attr('height', (d: any[]) => {
        return y(d[0]) - y(d[1]);
      })
      .attr('width', x.bandwidth())
      .on('mouseenter', (e: any, d: any) => {
        this.tooltip.show(e, d);
      })
      .on('mouseleave', (e: any, d: any) => {
        this.tooltip.hide();
      });
  }

  private setLegend(stackedData: any, colorScale: any): void {
    let legendDivs = d3
      .select('div#stackedBarChartPriveLegend')
      .selectAll('stackedBarLegendElement')
      .data(stackedData)
      .join('div')
      .attr('class', 'stackedBarLegendElement');

    legendDivs
      .append('svg')
      .attr('style', 'width: 15px; height: 15px')
      .append('rect')
      .attr('width', 15)
      .attr('height', 15)
      .attr('fill', (d: any) => colorScale(d.key) as string);

    legendDivs.append('span').text((d: any) => this.setLegendText(d.key));
  }

  private setLegendText(choice: string): string {
    if (choice === 'percentPrive') {
      return 'Pourcentage de vols privés';
    }
    if (choice === 'percentPublic') {
      return 'Pourcentage de vols commerciaux';
    }
    return 'Étiquette introuvable';
  }

  private getToolTipContent(e: any, d: any): string {
    const title =
      "<b style='font-size: 24px; font-weight: normal'>" + d.data.mois + '</b>';
    let subTitle = '';
    if (d[0] === 0) {
      const percentPrive = Math.round(d.data.percentPrive * 100) / 100;
      subTitle = subTitle.concat(
        `<p>Les vols privés comptent pour ${formatNumber(
          percentPrive,
          'fr-CA'
        )}% des vols du mois</p>`
      );
    } else {
      const percentPublic = Math.round(d.data.percentPublic * 100) / 100;
      subTitle = subTitle.concat(
        `<p>Les vols commerciaux comptent pour ${formatNumber(
          percentPublic,
          'fr-CA'
        )}% des vols du mois</p>`
      );
    }
    return title + '</br>' + subTitle;
  }
}
