import { Component, OnInit } from '@angular/core';

import * as d3 from 'd3';
import { FlightArrivalData } from 'src/assets/data/arrivalContinentFlightMatrix';
import { FlightDepartData } from 'src/assets/data/departContentFlightMatrix';

export interface Flight {
  Arrivee: string;
  Depart: string;
  Continent: string;
  Nombre: number;
}

@Component({
  selector: 'app-chord-chart',
  templateUrl: './chord-chart.component.html',
  styleUrls: ['./chord-chart.component.scss']
})

export class ChordChartComponent implements OnInit {
  
  private chordData:number[][] = [[]];
  
  private chordArrivalNames = ["Québec", "Amérique du Nord", "Europe", "Autres"];
  private chordDepartNames = ["Amérique du Nord", "Europe", "Autres", "Québec" ];
  private arrivalChordColors = [ "#ff7f0eff", "#1f77b4ff", "#2ca02cff", "#d62728ff"];
  private departChordColors = [ "#1f77b4ff", "#2ca02cff", "#d62728ff", "#ff7f0eff"];
  
  private chartContainerIdentifier = 'figure#chord'
  private svgWidth = 575;
  private svgHeight = 350;
  private chordInnerRadius: number = 0;
  private chordOuterRadius: number = 0;
  
  private svg: any;
  
  public isShowingArrival: boolean = true;
  public isShowingDataBox: boolean = false;
  
  public currentContinent: string = "";
  public currentContinentFlights: Flight[] = [];
  
  public arrivalContinentFlights: d3.InternMap<string, Flight[]> = new d3.InternMap();
  public departContinentFlights: d3.InternMap<string, Flight[]> = new d3.InternMap();
  
  constructor() { }
  
  ngOnInit(): void {
    this.processCountriesData()
    this.chordData = FlightArrivalData;
    this.chordInnerRadius = this.svgWidth * 0.24,
    this.chordOuterRadius = this.chordInnerRadius * 1.05;
    this.svg = this.instantiateSVGElement(this.chartContainerIdentifier, this.svgWidth, this.svgHeight)
    this.createChord(this.svg, this.chordInnerRadius, this.chordOuterRadius, this.chordArrivalNames, this.arrivalChordColors)
  }
  
  processCountriesData()
  {


    d3.json<Iterable<Flight>>('./assets/data/donneesArrivee.json')
    .then((data) => {
      if (data === undefined || data === null) {
        data = [];
      }
      this.arrivalContinentFlights = d3.group(data, d => d.Continent)
    });
    d3.json<Iterable<Flight>>('./assets/data/donneesDepart.json')
    .then((data) => {
      if (data === undefined || data === null) {
        data = [];
      }
      this.departContinentFlights = d3.group(data, d => d.Continent)
    });
  }
  
  showCountryBox(continent:string): void{
    let countries: Flight[] | undefined;
    if(this.isShowingArrival)
    {
      countries = this.arrivalContinentFlights.get(continent)
    }
    else{
      countries = this.departContinentFlights.get(continent)
    }
    if(countries !== undefined)
    {
      this.currentContinent = continent;
      this.currentContinentFlights = countries
      this.isShowingDataBox = true;
    }
  }
  
  hideCountryBox(): void {
    this.isShowingDataBox = false;
  }
  
  createChord(svg: any, innerRadius:number, outerRadius: number, elementNames: string[], chordColors: string[])
  {
    const chordDiagram = this.createChordDiagram(this.chordData)
    this.addChordLinks(svg, chordDiagram, innerRadius, chordColors,elementNames)
    this.addChordOuterLines(svg, chordDiagram, innerRadius, outerRadius, chordColors,elementNames )
    this.addElementsName(svg, chordDiagram, innerRadius, elementNames)
    return chordDiagram
  }
  
  public switchData(toArrival :boolean): void {
    if(toArrival && !this.isShowingArrival)
    {
      this.isShowingArrival = true;
      this.isShowingDataBox = false;
      this.chordData = FlightArrivalData;
      document.getElementById("chord")?.replaceChildren();
      this.svg = this.instantiateSVGElement(this.chartContainerIdentifier, this.svgWidth, this.svgHeight)
      this.createChord(this.svg, this.chordInnerRadius, this.chordOuterRadius, this.chordArrivalNames, this.arrivalChordColors)
    }
    else if(!toArrival && this.isShowingArrival)
    {
      this.isShowingArrival = false;
      this.isShowingDataBox = false;
      this.chordData = FlightDepartData;
      document.getElementById("chord")?.replaceChildren();
      this.svg = this.instantiateSVGElement(this.chartContainerIdentifier, this.svgWidth, this.svgHeight)
      this.createChord(this.svg, this.chordInnerRadius, this.chordOuterRadius, this.chordDepartNames, this.departChordColors)
    }
  }
  
  
  instantiateSVGElement(containerName: string, width: number, height: number): any {
    return d3
    .select(containerName)
    .append('svg')
    .attr('width', width)
    .attr('height', height)
    .append('g')
    .attr(
      'transform',
      'translate(' + width / 2 + ',' + height / 2 + ')'
      );
    }
    
    createChordDiagram(data: number[][], ): any {
      return d3.chord()
      .padAngle(0.05)
      .sortSubgroups(d3.descending)
      .sortChords(d3.descending)
      (data)
    }
    
    addChordOuterLines(svg: any, chord: any, innerRadius: number, outerRadius: number, chordColors: string[], elementNames: string[]): void 
    {
      const thisRef = this;
      svg
      .datum(chord)
      .append("g")
      .attr("id", "outer")
      .selectAll("g")
      .data(function(d: any) { return d.groups; })
      .enter()
      .append("g")
      .append("path")
      .style("fill", function(d:any){ return chordColors[d.index] })
      .style("stroke", "black")
      .style("cursor", "pointer")
      .attr('opacity', 0.7)
      .attr('fillOpacity', 0.7)
      .attr("d", d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      )
      .on("mouseover", (d:any) => this.chordMouseOver(svg, d.srcElement.__data__.index))
      .on("mouseout", (d:any) => this.chordMouseOut(svg))
      .on("mousedown", function(d:any) {thisRef.showCountryBox(elementNames[d.srcElement.__data__.index])});
    }
    
    addChordLinks(svg: any, chord: any, innerRadius: number, chordColors: string[], elementNames: string[]): void 
    {
      const thisRef = this;
      
      svg
      .datum(chord)
      .append("g")
      .attr("id", "inner")
      .selectAll("path")
      .data(function(d: any) { return d; })
      .enter()
      .append("path")
      .attr("d", d3.ribbon()
      .radius(innerRadius)
      )
      .style("fill", function(d:any){ let index: number = 0;
        index = d.source.value === 1000 ? d.target.index : d.source.index;
        return(chordColors[index]) })
        .style("stroke", "black")
        .style("cursor", "pointer")
        .attr('opacity', 0.7)
        .attr('fillOpacity', 0.7)
        .on("mouseover", (d:any) => this.chordMouseOver(svg, d.srcElement.__data__.source.index))
        .on("mouseout", (d:any) => this.chordMouseOut(svg))
        .on("mousedown", function(d:any){thisRef.showCountryBox(elementNames[d.srcElement.__data__.source.index]) });
      }
      
      addElementsName(svg: any, chord: any, innerRadius: number, elementNames: string[]): void 
      {
        var g = svg.select("g#outer")
        .data(chord.groups)
        .selectAll("g")
        
        .attr("class", function(d: any) {return "group " + elementNames[d.index];});
        
        g.append("svg:text")
        .each(function(d: any) { d.angle = (d.startAngle + d.endAngle) / 2; })
        .attr("dy", ".20em")
        .attr("id", "continent")
        .attr("class", "titles")
        .style("font-size", "19px")
        .attr("text-anchor", function(d: any) { return d.angle > Math.PI ? "end" : null; })
        .attr("transform", function(d: any) {
          return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
          + "translate(" + (innerRadius + 25) + ")"
          + "rotate(" + -(d.angle * 180 / Math.PI - 90) + ")"
        })
        .attr('opacity', 1)
        .attr('fillOpacity', 1)
        .text(function(d: any,i: any) { return elementNames[i]; }); 

        g.append("svg:text")
        .attr("id", "flights")
        .attr("text-anchor", "middle")
        .attr("dy", ".20em")
        .attr("class", "titles")
        .style("font-size", "23px")
        .style("font-weight", "bold")
        .attr('opacity', 0)
        .attr('fillOpacity', 0)
        .text(function(d: any) { return d.value + " vols"; }); 
      }
      
      chordMouseOver(svg:any, selectedIndex: any)
      {
        svg.selectAll("g#inner")
        .selectAll("path")
        .filter( (d:any) => d.source.index !== selectedIndex)
        .style("opacity", 0.3);
        
        svg.selectAll("g#outer")
        .selectAll("path")
        .filter( (d:any) => d.index !== selectedIndex)
        .style("opacity", 0.3);
        
        svg.selectAll("g#outer")
        .selectAll("text#continent")
        .filter( (d:any) => d.index !== selectedIndex)
        .style("opacity", 0.3);

        svg.selectAll("g#outer")
        .selectAll("text#flights")
        .filter( (d:any) => d.index === selectedIndex)
        .style("opacity", 1);
      }
      
      chordMouseOut(svg:any)
      {
        svg.selectAll("g#inner")
        .selectAll("path")
        .style("opacity", 0.7);
        
        svg.selectAll("g#outer")
        .selectAll("path")
        .style("opacity", 0.7);
        
        svg.selectAll("g")
        .selectAll("text#continent")
        .style("opacity", 1);

        svg.selectAll("g")
        .selectAll("text#flights")
        .style("opacity", 0);
      }
    }
     