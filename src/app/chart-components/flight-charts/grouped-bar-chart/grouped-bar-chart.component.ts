import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
const d3v6Tip = require('d3-v6-tip');
const { tip } = d3v6Tip;

@Component({
  selector: 'app-grouped-bar-chart',
  templateUrl: './grouped-bar-chart.component.html',
  styleUrls: ['./grouped-bar-chart.component.scss'],
})
export class GroupedBarChartComponent implements OnInit {
  private svg: any;
  private tooltip: any;
  private margin: number = 75;
  private width: number = 500 - this.margin * 2;
  private height: number = 400 - this.margin * 2;

  private topX: number = 5;
  private choice: string = 'Avions';

  constructor() {}

  ngOnInit(): void {
    this.createSvg();
    this.drawGroupBar();
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#groupBar1')
      .append('svg')
      .attr('width', this.width + this.margin * 2)
      .attr('height', this.height + this.margin * 2)
      .append('g')
      .attr('transform', 'translate(' + this.margin + ',' + this.margin + ')');

    this.tooltip = tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html((e: any, d: any, type: string) =>
        this.getToolTipContent(e, d, type)
      );

    this.svg.call(this.tooltip);
  }

  private drawGroupBar(): void {
    d3.json<Array<{ proprietaire: string; avions: number; vols: number }>>(
      './assets/data/avion-vols.json'
    ).then((data) => {
      if (data === undefined || data === null) {
        return;
      }

      if (this.choice === 'Avions') {
        data.sort((a, b) => a.avions - b.avions).reverse();
        data = data.slice(0, this.topX);
      }
      if (this.choice === 'Vols') {
        data.sort((a, b) => a.vols - b.vols).reverse();
        data = data.slice(0, this.topX);
      }

      const scaleX = this.setScaleX(data);
      this.drawXAxis(scaleX);
      const subGroupXScale = this.setSubScaleX(scaleX);

      const scaleY = this.setScaleY(data);
      this.drawYAxis(scaleY);

      const colorScale = this.setColorScale();

      this.svg.selectAll('g.groupsAV').remove();

      this.fillGroupedBars(data, scaleX, subGroupXScale, scaleY, colorScale);
    });
  }

  private setScaleX(data: any): any {
    return d3
      .scaleBand()
      .padding(0.1)
      .range([0, this.width])
      .domain(
        data.map((d: { [x: string]: any }) => d['proprietaire'] ?? 'unknown')
      );
  }

  private setScaleY(data: any): any {
    return d3
      .scaleLog()
      .range([this.height, 0])
      .domain([
        1,
        d3.max(data, (d: any) =>
          d.avions! > d.vols! ? (d.avions! as number) : (d.vols! as number)
        ) as number,
      ]);
  }

  private setSubScaleX(scaleX: any): any {
    return d3
      .scaleBand()
      .padding(0.025)
      .domain(['avions', 'vols'])
      .range([0, scaleX.bandwidth()]);
  }

  private setColorScale(): any {
    return d3.scaleOrdinal().range(['#f2980e', '#1f77b3']).domain(['vols', 'avions']);
  }

  private drawXAxis(x: any): void {
    this.svg.selectAll('g.axisXGroupBarChart').remove();

    this.svg
      .append('g')
      .attr('transform', 'translate(0,' + this.height + ')')
      .attr('class', 'axisXGroupBarChart')
      .call(d3.axisBottom(x))
      .selectAll('text')
      .attr('transform', 'translate(-5,-5)rotate(-25)')
      .style('text-anchor', 'end');
  }

  private drawYAxis(y: any): void {
    this.svg.select('axisYGroupBarChart').remove();

    this.svg
      .append('g')
      .call(d3.axisLeft(y))
      .attr('class', 'axisYGroupBarChart');
  }

  private fillGroupedBars(
    data: any,
    scaleX: any,
    subGroupXScale: any,
    scaleY: any,
    colorScale: any
  ): void {
    const group = this.svg
      .selectAll('groupsAV')
      .data(data)
      .enter()
      .append('g')
      .attr('class', 'groupsAV')
      .attr(
        'transform',
        (d: { proprietaire: any }) => `translate(${scaleX(d.proprietaire)},0)`
      );

    group
      .selectAll('groupsAV')
      .data((data: any) => [data])
      .enter()
      .append('rect')
      .attr('class', 'groupBarAvions')
      .style('fill', () => colorScale('avions'))
      .attr('x', (d: any) => subGroupXScale('avions'))
      .attr('y', (d: { avions: any }) => scaleY(d.avions))
      .attr('width', subGroupXScale.bandwidth())
      .attr('height', (d: { avions: any }) => {
        return this.height - scaleY(d.avions);
      })
      .on('mouseenter', (e: any, d: any) => {
        this.tooltip.show(e, d, 'avions');
      })
      .on('mouseleave', (e: any, d: any) => {
        this.tooltip.hide();
      });

    group
      .selectAll('groupsAV')
      .data((data: any) => [data])
      .enter()
      .append('rect')
      .attr('class', 'groupBarVols')
      .style('fill', () => colorScale('vols'))
      .attr('x', (d: any) => subGroupXScale('vols'))
      .attr('y', (d: { vols: any }) => scaleY(d.vols))
      .attr('width', subGroupXScale.bandwidth())
      .attr('height', (d: { vols: any }) => {
        return this.height - scaleY(d.vols);
      })
      .on('mouseenter', (e: any, d: any) => {
        this.tooltip.show(e, d, 'vols');
      })
      .on('mouseleave', (e: any, d: any) => {
        this.tooltip.hide();
      });
  }

  private getToolTipContent(e: any, d: any, type: string): string {
    const title =
      "<b style='font-size: 24px; font-weight: normal'>" +
      d.proprietaire +
      '</b>';
    let subTitle = '';
    if (type === 'avions') {
      subTitle = subTitle.concat(
        `<p>La compagnie aérienne possède ${d.avions} avions</p>`
      );
    }
    if (type === 'vols') {
      subTitle = subTitle.concat(
        `<p>La compagnie aérienne a effectué ${d.vols} vols</p>`
      );
    }
    return title + '</br>' + subTitle;
  }

  changeTop(top: string): void {
    this.topX = parseInt(top);
    this.drawGroupBar();
  }

  changeOrder(order: string): void {
    this.choice = order;
    this.drawGroupBar();
  }
}
