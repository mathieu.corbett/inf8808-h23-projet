import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PolarChart2Component } from './polar-chart2.component';

describe('PolarChart2Component', () => {
  let component: PolarChart2Component;
  let fixture: ComponentFixture<PolarChart2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PolarChart2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PolarChart2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
