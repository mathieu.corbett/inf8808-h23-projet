import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-polar-chart2',
  templateUrl: './polar-chart2.component.html',
  styleUrls: ['./polar-chart2.component.scss'],
})
export class PolarChart2Component implements OnInit {
  private svg: any;
  private margin = 50;
  private width = 400;
  private height = 400;
  private innerRadius = 80;
  private outerRadius = 200;
  private textRadius = 70;

  constructor() {}

  ngOnInit(): void {
    this.createSvg();
    this.drawPolar();
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#polar2')
      .append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .append('g')
      .attr(
        'transform',
        'translate(' +
          (this.width / 2 + this.margin / 2) +
          ',' +
          (this.height / 2 - this.margin / 2) +
          ')'
      );
  }

  private drawPolar(): void {
    d3.json<Iterable<{ time: string; count: number }>>(
      './assets/data/arrivals.json'
    ).then((data) => {
      if (data === undefined || data === null) {
        data = [];
      }

      const maxFlights = d3.max(d3.map(data, (d) => d.count)) ?? 0;

      const x = d3
        .scaleBand()
        .domain(d3.map(data, (d) => d.time))
        .range([0, 2 * Math.PI]);

      const y = d3
        .scaleRadial()
        .domain([0, maxFlights])
        .range([this.innerRadius, this.outerRadius]);

      // Draw the bars
      this.svg
        .append('g')
        .selectAll('path')
        .data(data)
        .enter()
        .append('path')
        .attr('fill', '#1f77b3')
        .attr(
          'd',
          d3
            .arc()
            .innerRadius(this.innerRadius)
            .outerRadius((d: any) => y(d.count))
            .startAngle((d: any) => (x(d.time) ?? 0) - x.bandwidth() / 2)
            .endAngle((d: any) => (x(d.time) ?? 0) + x.bandwidth() / 2)
            .padAngle(0.01)
            .padRadius(this.innerRadius)
        );

      // Draw center number
      this.svg
        .append('text')
        .attr('id', 'countLabel')
        .attr('transform', 'translate(0,0)')
        .attr('text-anchor', 'middle')
        .attr('dominant-baseline', 'middle');

      d3.selectAll('figure#polar2 path')
        .on('mouseover', function (e: MouseEvent, d: any) {
          d3.select(this).attr('fill', '#2ca9ff');
          d3.select('figure#polar2 #countLabel').text(d.count + " vols");
        })
        .on('mouseout', function () {
          d3.select(this).attr('fill', '#1f77b3');
          d3.select('figure#polar2 #countLabel').text('');
        });

      // Draw the labels
      this.svg
        .append('g')
        .selectAll('g')
        .data(data)
        .enter()
        .append('g')
        .attr(
          'transform',
          (d: any) =>
            'translate(' +
            this.textRadius * Math.sin(x(d.time) ?? 0) +
            ',' +
            -this.textRadius * Math.cos(x(d.time) ?? 0) +
            ')'
        )
        .append('text')
        .text((d: any) => d.time + "h")
        .style('font-size', '12px')
        .attr('text-anchor', 'middle')
        .attr('alignment-baseline', 'middle');
    });
  }
}
