import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PolarChart1Component } from './polar-chart1.component';

describe('PolarChart1Component', () => {
  let component: PolarChart1Component;
  let fixture: ComponentFixture<PolarChart1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PolarChart1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PolarChart1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
