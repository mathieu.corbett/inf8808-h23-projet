import { Component, OnInit } from '@angular/core';
import { PieChart3Data } from '../../../../assets/data/pie-chart-data';
import * as d3 from 'd3';

@Component({
  selector: 'app-pie-chart3',
  templateUrl: './pie-chart3.component.html',
  styleUrls: ['./pie-chart3.component.scss'],
})
export class PieChart3Component implements OnInit {
  private data: any = [];
  private svg: any;
  private margin = 50;
  private width = 400;
  private height = 400;
  // The radius of the pie chart is half the smallest side
  private radius = Math.min(this.width, this.height) / 2 - this.margin;
  private colors: any;

  constructor() {}

  ngOnInit(): void {
    this.data = PieChart3Data;
    this.createSvg();
    this.createColors();
    this.drawChart();
  }

  private createSvg(): void {
    this.svg = d3
      .select('figure#pie3')
      .append('svg')
      .attr('width', this.width)
      .attr('height', this.height)
      .append('g')
      .attr(
        'transform',
        'translate(' + this.width / 2 + ',' + this.height / 2 + ')'
      );
  }

  private createColors(): void {
    this.colors = d3
      .scaleOrdinal()
      .domain(this.data.map((d: any) => d.Tonnes.toString()))
      .range(['#fe7f0e', '#1f77b3']);
  }

  private drawChart(): void {
    // Compute the position of each group on the pie:
    const pie = d3.pie<any>().value((d: any) => Number(d.Tonnes));

    // Build the pie chart
    this.svg
      .selectAll('pieces')
      .data(pie(this.data))
      .enter()
      .append('path')
      .attr('d', d3.arc().innerRadius(0).outerRadius(this.radius))
      .attr('fill', (d: any, i: any) => this.colors(i))
      .attr('stroke', '#121926')
      .style('stroke-width', '1px');

    // Add labels
    const labelLocation = d3.arc().innerRadius(100).outerRadius(this.radius);

    this.svg
      .selectAll('pieces')
      .data(pie(this.data))
      .enter()
      .append('text')
      .attr('dy', '0em')
      .text((d: any) => d.data.Type)
      .attr(
        'transform',
        (d: any) =>
          'translate(' +
          (labelLocation.centroid(d)[0] * 6) / 10 +
          ',' +
          (labelLocation.centroid(d)[1] * 6) / 10 +
          ')'
      )
      .attr('fill', function () {
        return '#FFFFFF';
      })
      .attr('font-weight', function () {
        return 900;
      })
      .attr('stroke', (d: any) => this.colors(d))
      .attr('stroke-width', (d: any) => '0.5px')
      .style('text-anchor', 'middle')
      .style('font-size', '20px');

    this.svg
      .selectAll('pieces')
      .data(pie(this.data))
      .enter()
      .append('text')
      .attr('dy', '1em')
      .text((d: any) => d.data.Text)
      .attr(
        'transform',
        (d: any) =>
          'translate(' +
          (labelLocation.centroid(d)[0] * 6) / 10 +
          ',' +
          (labelLocation.centroid(d)[1] * 6) / 10 +
          ')'
      )
      .attr('fill', function () {
        return '#FFFFFF';
      })
      .attr('font-weight', function () {
        return 900;
      })
      .attr('stroke', (d: any) => this.colors(d))
      .attr('stroke-width', (d: any) => '0.5px')
      .style('text-anchor', 'middle')
      .style('font-size', '20px');
  }
}
