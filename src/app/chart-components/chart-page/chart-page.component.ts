import { Component, Input, OnInit } from '@angular/core';
import { ChartEnum } from '../../enums/chart-enum';
import { QuestionSectionEnum } from '../../enums/question-section-enum';
import { NumberVizEnum } from '../../enums/number-viz-enum';
import * as d3 from 'd3';

@Component({
  selector: 'app-chart-page',
  templateUrl: './chart-page.component.html',
  styleUrls: ['./chart-page.component.scss'],
})
export class ChartPageComponent implements OnInit {
  readonly ChartEnum = ChartEnum;
  readonly QuestionSectionEnum = QuestionSectionEnum;
  readonly NumberVizEnum = NumberVizEnum;
  @Input() Chart: ChartEnum = ChartEnum.Bar1;
  @Input() Section: QuestionSectionEnum = QuestionSectionEnum.GES;
  @Input() NumberViz: NumberVizEnum | null = null;

  public gesDescription: string =
    "Cette section présente une vue sur les effets des vols aériens sur les émissions de gaz à effet de serre. Les différentes représentations visuelles démontrent l’ampleur de l’impact que l’aviation au Québec peut avoir sur cette problématique majeure. La section présente les quantités de gaz à effet de serre rejetées par les vols d'avions en faisant la comparaison entre les gros et petits aéronefs.";
  public planesDescription: string =
    "Cette section a pour but de faire ressortir les caractéristiques des avions en circulation au Québec. Divers graphiques sont présentés qui abordent la proportion d'utilisation des avions selon le type d'aéronef, le modèle, le fabricant et le propriétaire. Ceci permet d'avoir un aperçu sur les dynamiques et les préférences présentes dans le domaine de l'aéronautique au Québec";
  public flightsDescription: string =
    "Cette section vient mettre en évidence les différentes caractéristiques portant sur les vols en eux-mêmes. Les différentes représentations comparent l'impact des vols privés et commerciaux, ainsi que l'impact des différentes compagnies aériennes. Par la suite, cette section aborde des caractéristiques plus générales des vols: leur heure de départ ou d'arrivée, leur destination, etc.";

  public gesTitle: string = 'Impact des GES';
  public planesTitle: string = 'Avions du Québec';
  public flightsTitle: string = 'Vols des avions';

  public emissionNumberViz: string = '12,63 tonnes';
  public distanceNumberViz: number = -1;

  constructor() {
    d3.json('./assets/data/volDistance.json').then((data: any) => {
      let distances = d3.map(data, (d: any) => parseInt(d.distance));
      distances = d3.filter(distances, (d: any) => !Number.isNaN(d));
      let average = Math.round(d3.mean(distances)!!);

      this.timedCounter(average, 2, (value: number) => {
        this.distanceNumberViz = Math.round(value);
      });
    });
  }

  ngOnInit(): void { }

  private timedCounter(finalValue: number, seconds: number, callback: any) {
    var startTime = new Date().getTime();
    var milliseconds = seconds * 1000;

    (function update() {
      var currentTime = new Date().getTime();
      var value = (finalValue * (currentTime - startTime)) / milliseconds;

      if (value >= finalValue) value = finalValue;
      else setTimeout(update, 0);

      callback && callback(value);
    })();
  }
}
