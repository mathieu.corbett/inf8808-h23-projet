import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr-CA';
registerLocaleData(localeFr);
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SlideComponent } from './slide/slide.component';
import { HomeComponent } from './home/home.component';
import { GroupedBarChartComponent } from './chart-components/flight-charts/grouped-bar-chart/grouped-bar-chart.component';
import { ChordChartComponent } from './chart-components/flight-charts/chord-chart/chord-chart.component';
import { PolarChart1Component } from './chart-components/flight-charts/polar-chart1/polar-chart1.component';
import { StackedBarChartComponent } from './chart-components/flight-charts/stacked-bar-chart/stacked-bar-chart.component';
import { PieChart1Component } from './chart-components/GES-charts/pie-chart1/pie-chart1.component';
import { PieChart2Component } from './chart-components/GES-charts/pie-chart2/pie-chart2.component';
import { PieChart3Component } from './chart-components/GES-charts/pie-chart3/pie-chart3.component';
import { BarChart1Component } from './chart-components/plane-charts/bar-chart1/bar-chart1.component';
import { BarChart2Component } from './chart-components/plane-charts/bar-chart2/bar-chart2.component';
import { BubbleChart1Component } from './chart-components/plane-charts/bubble-chart1/bubble-chart1.component';
import { BubbleChart2Component } from './chart-components/plane-charts/bubble-chart2/bubble-chart2.component';
import { ChartPageComponent } from './chart-components/chart-page/chart-page.component';
import { PolarChart2Component } from './chart-components/flight-charts/polar-chart2/polar-chart2.component';


@NgModule({
  declarations: [
    AppComponent,
    SlideComponent,
    HomeComponent,
    GroupedBarChartComponent,
    ChordChartComponent,
    PolarChart1Component,
    StackedBarChartComponent,
    PieChart1Component,
    PieChart2Component,
    PieChart3Component,
    BarChart1Component,
    BarChart2Component,
    BubbleChart1Component,
    BubbleChart2Component,
    ChartPageComponent,
    PolarChart2Component,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [
    {provide: LOCALE_ID, useValue: 'fr-CA'},
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
