import { Component, OnInit } from '@angular/core';
import Reveal from 'reveal.js';
import { ChartEnum } from '../enums/chart-enum';
import { QuestionSectionEnum } from '../enums/question-section-enum';
import { NumberVizEnum } from '../enums/number-viz-enum';

//import RevealMarkdown from 'reveal.js/plugin/markdown/markdown.js';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss'],
})
export class SlideComponent implements OnInit {
  readonly ChartEnum = ChartEnum;
  readonly QuestionSectionEnum = QuestionSectionEnum;
  readonly NumberVizEnum = NumberVizEnum;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    Reveal.initialize({
      parallaxBackgroundImage: '',
      parallaxBackgroundSize: '',
      parallaxBackgroundHorizontal: 200,
      parallaxBackgroundVertical: 50,
    });
  }
}
