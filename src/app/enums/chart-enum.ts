export enum ChartEnum {
  Pie1 = 0,
  Pie2,
  Pie3,
  Bar1,
  Bar2,
  Bubble1,
  Bubble2,
  Chord,
  GroupedBar,
  StackedBar,
  Polar1,
  Polar2,
}
