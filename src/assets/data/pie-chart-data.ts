export var PieChart1Data = [
  { Type: 'Gros avions', Quantité: '760', Text: '760 vols' },
  { Type: 'Petits avions', Quantité: '77190', Text: '77 190 vols' },
];

export var PieChart2Data = [
  { Type: 'Gros avions', Heures: '3286', Text: '3286 heures de vol' },
  { Type: 'Petits avions', Heures: '184054', Text: '184 054 heures de vol' },
];

export var PieChart3Data = [
  { Type: 'Gros avions', Tonnes: '37371.39', Text: '37 371,39 Tonnes de GES' },
  {
    Type: 'Petits avions',
    Tonnes: '946970.74',
    Text: '946 970,74 Tonnes de GES',
  },
];
